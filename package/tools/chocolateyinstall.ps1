$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$url        = 'https://download.tuxfamily.org/manaplus/download/1.8.12.8/windows/manaplus-1.8.12.8-win32.exe'
$url64      = 'https://download.tuxfamily.org/manaplus/download/1.8.12.8/windows/manaplus-1.8.12.8-win64.exe'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64

  softwareName  = 'ManaPlus*'

  checksum      = 'DD72A30A06778991664D996A0AF2AFDAC160701FF3911FD2F951AFF1A949BDE4'
  checksumType  = 'sha256'
  checksum64    = '1B411F134E5489F5C80A44F3B7F839EC91791ED6A9585758089A311914C7AF20'
  checksumType64= 'sha256'

  silentArgs   = '/S'
  validExitCodes= @(0)
}

Install-ChocolateyPackage @packageArgs
